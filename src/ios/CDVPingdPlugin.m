
#import "CDVPingdPlugin.h"

@interface CDVPingdPlugin (PrivateMethods)
- (void)updateOnlineStatus;
@end

@implementation CDVPingdPlugin

@synthesize state, level, callbackId, isPlugged;


- (void)updateBatteryStatus:(CDVInvokedUrlCommand*)command
{
    self.callbackId = command.callbackId;
    NSInteger* batteryData = [self getBatteryStatus];
    //NSLog(@"This is an int: %td", batteryData);
    
    if (self.callbackId) {
        NSLog(@"======return");
        CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:batteryData];
        [result setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
    }
}

- (NSInteger*)getBatteryStatus
{
    NSLog(@"====getBatteryStatus");
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    UIDevice* currentDevice = [UIDevice currentDevice];
    float currentLevel = [currentDevice batteryLevel];
    
    NSInteger intValue = (NSInteger) roundf(currentLevel*100);

    NSLog(@"This is an int: %td", intValue);
    
    return intValue;
}


@end
