#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <UIKit/UIKit.h>

@interface CDVPingdPlugin : CDVPlugin {
    UIDeviceBatteryState state;
    float level;
    bool isPlugged;
    NSString* callbackId;
}

@property (nonatomic) UIDeviceBatteryState state;
@property (nonatomic) float level;
@property (nonatomic) bool isPlugged;
@property (strong) NSString* callbackId;

- (void)updateBatteryStatus:(CDVInvokedUrlCommand*)command;
- (NSInteger*)getBatteryStatus;
@end
