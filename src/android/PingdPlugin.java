
package com.tem.pingdplugin;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.content.SharedPreferences;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.cordova.CordovaWebView;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;

public class PingdPlugin extends CordovaPlugin {

    private static final String LOG_TAG = "BatteryManager";
    private BatteryService service;
    //BroadcastReceiver receiver;
    public static CallbackContext batteryCallbackContext;
    //private static CordovaWebView gWebView;

    /**
     * Executes the request.
     *
     * @param action        	The action to execute.
     * @param args          	JSONArry of arguments for the plugin.
     * @param callbackContext 	The callback context used when calling back into JavaScript.
     * @return              	True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {
        batteryCallbackContext = callbackContext;
        Context context = cordova.getActivity();
        Intent i = new Intent(cordova.getActivity(), BatteryService.class);
        ComponentName component = new ComponentName(context, ScreenReceiver.class);
        int status = context.getPackageManager().getComponentEnabledSetting(component);

        SharedPreferences sharedPref = context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        if(action.equals("startMonitoring")){
            
            try{
                editor.putString("token",args.getString(0));
                editor.putString("apiUrl",args.getString(1));
                editor.commit();
                batteryCallbackContext.success();
            }catch (Throwable e) {
                e.printStackTrace();
                batteryCallbackContext.error("Could not save token: "+e.getMessage());
            }

            if(status == PackageManager.COMPONENT_ENABLED_STATE_ENABLED) {
                Log.d("service","receiver is enabled");
            } 
            else if(status == PackageManager.COMPONENT_ENABLED_STATE_DISABLED) {
                Log.d("service","receiver is disabled");
            }
            //if (service == null) {
                // start service
            //}    
            //Enable
            context.getPackageManager().setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_ENABLED , PackageManager.DONT_KILL_APP);
            cordova.getActivity().startService(i);
        }
        else if(action.equals("stopMonitoring")){
            
            editor.clear();
            editor.commit();
            //Disable
            context.getPackageManager().setComponentEnabledSetting(component, PackageManager.COMPONENT_ENABLED_STATE_DISABLED , PackageManager.DONT_KILL_APP);
            cordova.getActivity().stopService(i);
        }
        else if(action.equals("updateBatteryStatus")){
            
            batteryCallbackContext.success(getBatteryLevel());
        }

        PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
        pluginResult.setKeepCallback(true);
        batteryCallbackContext.sendPluginResult(pluginResult);

        return true;

        /*else if(action.equals("getToken")){
            SharedPreferences sharedPref = context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
            String token = sharedPref.getString("token", "null");
            JSONObject obj = new JSONObject();
            try {
                obj.put("token", token);
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
            }
            PluginResult result = new PluginResult(PluginResult.Status.OK, obj);
            result.setKeepCallback(true);
            if(batteryCallbackContext != null){
                batteryCallbackContext.sendPluginResult(result);
            }
            //batteryCallbackContext.success(token);
        }*/
    }
    public int getBatteryLevel() {
        Intent batteryIntent = cordova.getActivity().registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if(level == -1 || scale == -1) {
            return 50;
        }
        int battery = (int) (((float)level / (float)scale) * 100);
        return battery;
    }
}
