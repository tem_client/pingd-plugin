
package com.tem.pingdplugin;

import android.content.BroadcastReceiver;
import android.content.SharedPreferences;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class ScreenReceiver extends BroadcastReceiver {

    @ Override
    public void onReceive(Context context, Intent intent) {
        //Toast.makeText(context, "onReceive", Toast.LENGTH_LONG).show();
        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
			
            SharedPreferences sharedPref = context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
            String token = sharedPref.getString("token", "null");
            String apiUrl = sharedPref.getString("apiUrl", "null");
            //Toast.makeText(context,"Open Screen - " +apiUrl,Toast.LENGTH_SHORT).show();
            if(!token.equals("null") && !apiUrl.equals("null")){
                new UpdateUserAlive().execute(token, apiUrl);
            }
        }
    }

    class UpdateUserAlive extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(params[1] + "v1/users/me/alive");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Authorization", params[0]);
                conn.setReadTimeout(10000  ); //milliseconds
                conn.setConnectTimeout(15000 ); //milliseconds
                conn.setRequestMethod("POST");
                conn.setDoInput(true);

                int responseCode=conn.getResponseCode();
                Log.i("resonse",""+responseCode);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

            }
            return "";
        }

        @Override
        protected void onPostExecute(String bitmap) {

        }
    }
}
