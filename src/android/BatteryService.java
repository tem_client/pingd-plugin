package com.tem.pingdplugin;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class BatteryService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MyService", "onStartCommand");
        // do not receive all available system information (it is a filter!) 
        final IntentFilter battChangeFilter = new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED);
        // register our receiver 
        this.registerReceiver(this.batteryChangeReceiver, battChangeFilter);
        return super.onStartCommand(intent, flags, startId);
    }

    private final BroadcastReceiver batteryChangeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
                final int currLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                final int maxLevel = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                final int percentage = (int) Math.round((currLevel * 100.0) / maxLevel);

                SharedPreferences sharedPref = context.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
                String token = sharedPref.getString("token", "null");
                int battery = sharedPref.getInt("battery", -1);
                String apiUrl = sharedPref.getString("apiUrl", "null");

                boolean isPlugged = intent.getIntExtra(android.os.BatteryManager.EXTRA_PLUGGED, -1) > 0 ? true : false;
                boolean isPluggedShared = sharedPref.getBoolean("isPlugged", false);


                if(battery!=percentage || isPluggedShared!=isPlugged) {
                //if(battery!=percentage) {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("battery", percentage);
                    editor.putBoolean("isPlugged", isPlugged);
                    editor.commit();
                    //Toast.makeText(context, "battery: " + percentage+" , is plugged: "+isPlugged, Toast.LENGTH_LONG).show();
                    //Toast.makeText(context, "battery: " + percentage, Toast.LENGTH_LONG).show();
                    if(!token.equals("null") && !apiUrl.equals("null")){
                        new UpdateUserBattery().execute(token, String.valueOf(percentage), apiUrl);
                    }
                    
                }
                //checkBatteryLevel(intent);
            }
        }
    };
    @Override
    public void onDestroy() {
        unregisterReceiver(this.batteryChangeReceiver);
        //Toast.makeText(this, "Destroy", Toast.LENGTH_LONG).show();
    }
    @Override
    public IBinder onBind(Intent intent) {
        // There are Bound an Unbound Services - you should read something about 
        // that. This one is an Unbounded Service. 
        return null;
    }

    class UpdateUserBattery extends AsyncTask<String, Void, String> {

        @Override
        // Actual download method, run in the task thread
        protected String doInBackground(String... params) {
            try {

                URL url = new URL(params[2] + "v1/users/me/battery");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Authorization", params[0]);
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                HashMap<String,String> postDataParams = new HashMap<String,String>();
                postDataParams.put("battery",params[1]);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();
                int responseCode=conn.getResponseCode();
                Log.i("resonse",""+responseCode);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {

            }
            return "";
        }

        @Override
        protected void onPostExecute(String bitmap) {
        }
    }
    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }
}