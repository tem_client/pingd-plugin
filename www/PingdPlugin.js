//var exec = cordova.require('cordova/exec');


function PingdPlugin() {
  console.log("PingdPlugin.js: is created");
}

PingdPlugin.prototype.startMonitoring = function (successCallback, errorCallback, token, apiUrl) {
 
  cordova.exec(successCallback,errorCallback,"PingdPlugin", "startMonitoring", [token, apiUrl]);
  
}

PingdPlugin.prototype.stopMonitoring = function (successCallback, errorCallback) {
 
  cordova.exec(successCallback,errorCallback,"PingdPlugin", "stopMonitoring", []);
  
}

PingdPlugin.prototype.getBattery = function (successCallback, errorCallback) {
 
  cordova.exec(successCallback,errorCallback,"PingdPlugin", "updateBatteryStatus", []);
  
}

var pingdPlugin = new PingdPlugin();
module.exports = pingdPlugin;
